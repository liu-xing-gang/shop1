/**
 * Created by Liu Xinggang
 */

$(function () {

    //编辑商品
    function editGoods() {

    }

    //商品数量增减
    function changeAmount(n, f) {
        f == true ? n++ : n--
        n = n <= 1 ? 1 : n
        return n;
    }

    //单选
    function selectGoods(o) {
        var flag = o.find('input:radio').attr("checked")
        flag = !flag
        o.toggleClass('check')
        o.find('input:radio').attr("checked", flag)
    }

    //全选
    function selectAll(o) {
        o.toggleClass('check')
        checkAllFlag = !checkAllFlag
        if (checkAllFlag == true) {
            o.addClass('check')
            o.find('input:radio').attr("checked", true)
            return
        }
        o.removeClass('check')
        o.find('input:radio').attr("checked", false)
    }

    //删除商品
    function delGoods(o, index) {
        layer.open({
            content: '是否确认删除此商品？'
            , btn: ['确认', '取消']
            , yes: function (i) {
                console.log(o, index)
                layer.close(i)
                o.slideUp("normal",function(){
                    o.remove()
                    changeMoney()
                });
            }
        });
    }

    //收藏商品
    function collectGoods(o, index) {
        layer.open({
            content: '是否确认将此商品移至收藏？'
            , btn: ['确认', '取消']
            , yes: function () {
                console.log(o, index)

                layer.open({
                    content: '收藏成功'
                    , time: 1
                    , skin: 'msg'
                });
            }
        });
    }

    //计算金额
    function changeMoney(flag) {
        calcTotalPrice()  
    }

    //计算总价
    function calcTotalPrice(o, price, number) {
        var arrLine = $('.item-check-btn.check').not('#checkall').closest('.line'),
            totalPrice = new Number
        arrLine.each(function () {
            var price = parseFloat($(this).find('.price i').text()),
                number = parseInt($(this).find('.amount').val())
            totalPrice += price * number
            console.log('单价' + price, '数量' + number, '总价四舍五入2位小数' + totalPrice.toFixed(2), '未四舍五入' + totalPrice)
        })
        $('.selectprice i').text(totalPrice.toFixed(2))
    }

    function touchStart(e, o) {
        // 记录开始位置
        startX = e.originalEvent.changedTouches[0].pageX
    }

    function touchMove(e, o) {
        // 记录开始位置
        var move = startX - e.originalEvent.changedTouches[0].pageX
        o.css({ 'transform': 'translateX(-' + move + 'px)', 'transition': '.3s linear' })
    }

    function touchEnd(e, o) {
        var endX = e.originalEvent.changedTouches[0].pageX,
            disX = startX - endX
        //如果距离小于,强行回到起点
        if (disX < wd) {
            o.css({ 'transform': 'translateX(0px)', 'transition': '.3s linear' })
            // console.log(e.target.style)
        } else {
            //大于 滑动到最大值
            o.css({ 'transform': 'translateX(-' + moveX + 'px)', 'transition': '.3s linear' })
        }
    }


    var checkAllFlag = true,
        wd = 50,
        startX = 0,
        moveX = $('.del-action').width()

    //点击右上角 ‘编辑, 完成’ 切换底部菜单
    $('.btn-edit').on('click', function () {
        $('.cart-end').toggleClass('active')
        $('.info-group').toggleClass('active')
    })

    $('.btn-finish').on('click', function () {
        $('.cart-end').toggleClass('active')
        $('.info-group').toggleClass('active')
    })

    //全选/取消全选
    $('#checkall').on('click', function () {
        selectAll($('.item-check-btn'))

        var o = $(this)
        var flag = o.find('input:radio').attr("checked")
        flag = !flag
        o.toggleClass('check')
        o.find('input:radio').attr("checked", flag)
    })

    //单选
    $('.item-check-btn').on('click', function () {
        selectGoods($(this))
        calcTotalPrice()
    })

    //数量+
    $('.btn-add').on('click', function () {
        var o = $(this).closest('.btn-control').find('.amount'),
            val = o.val(),
            n = changeAmount(val, true)
        o.val(n)
        changeMoney()
    })

    //数量-
    $('.btn-del').on('click', function () {
        var o = $(this).closest('.btn-control').find('.amount'),
            val = o.val(),
            n = changeAmount(val)
        o.val(n)
        changeMoney()
    })

    //滑动显示 ’收藏删除‘
    $('.line').on('touchstart', function (e) {
        touchStart(e, $(this))
    })

    $('.line').on('touchmove', function (e) {
        touchMove(e, $(this))
    })

    $('.line').on('touchend', function (e) {
        touchEnd(e, $(this))
    })

    //收藏商品
    $('.btn-collect').on('click', function () {
        var o = $(this).closest('.cart li'),
            i = o.index()
        collectGoods(o, i)
    })

    //删除单项商品
    $('.btn-remove').on('click', function () {
        var o = $(this).closest('.cart li'),
            i = o.index()
        delGoods(o, i)
    })

    //点击底部加入收藏
    $('.btn-to-collect').on('click', function () {
        var o = $('.item-check-btn.check').not('#checkall')
        collectGoods(o)
    })

    //点击底部删除
    $('.btn-to-del').on('click', function () {
        var o = $('.item-check-btn.check').not('#checkall').closest('li')
        delGoods(o)
    })


})
