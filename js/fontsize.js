/**
 * Created by Liu Xinggang
 */

//获取当前页面根元素的font-size
function resize() {
    var deviceWidth = document.documentElement.clientWidth
    if(deviceWidth >= 750) deviceWidth = 750
    document.documentElement.style.fontSize = deviceWidth / 7.5 + 'px';
}
resize();
window.addEventListener('resize', resize);
window.onload = function(){
    //返回上一页
    function back(ele) {
        $(ele).click(function() {
            window.history.back(-1);
        })
    }
    back("#back");
}