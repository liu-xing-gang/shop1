/**
 * Created by Liu Xinggang
 */
$(function () {
    function isPassive() {
        var supportsPassiveOption = false;
        try {
            addEventListener("test", null, Object.defineProperty({}, 'passive', {
                get: function () {
                    supportsPassiveOption = true;
                }
            }));
        } catch (e) { }
        return supportsPassiveOption;
    }

    /**
     * 初始化iscroll
     */
    var myScroll, myScroll
    function loaded() {
        myScroll = new IScroll('.iscroll-goods');
        myScroll2 = new IScroll('.iscroll-category');
    }

    document.addEventListener('touchmove', function (e) { e.preventDefault(); }, isPassive() ? {
        capture: false,
        passive: false
    } : false);

    $(window).on('load', function () {
        /**
         * 
         * ===================================================
         * 此处与category不同
         * ===================================================
         * 
         */
        var hGoods = $(window).height() - $('.am-title').outerHeight() - $('.am-navbar').outerHeight()
        $('.iscroll-goods').css({ 'height': hGoods })
        $('.iscroll-category').css({ 'height': hGoods })
        loaded()
    })

    /**
     * 点击显示对应商品列表
     */
    $('.iscroll-category li').each(function (index) {
        var _this = $(this).find('.c-navbar-item')
        var startX, startY
        _this.on("touchstart", function (e) {
            // 判断默认行为是否可以被禁用
            if (e.cancelable) {
                // 判断默认行为是否已经被禁用
                if (!e.defaultPrevented) {
                    e.preventDefault();
                }
            }
            startX = e.originalEvent.changedTouches[0].pageX,
                startY = e.originalEvent.changedTouches[0].pageY
        });
        _this.on("touchend", function (e) {
            // 判断默认行为是否可以被禁用
            if (e.cancelable) {
                // 判断默认行为是否已经被禁用
                if (!e.defaultPrevented) {
                    e.preventDefault();
                }
            }
            var moveEndX = e.originalEvent.changedTouches[0].pageX,
                moveEndY = e.originalEvent.changedTouches[0].pageY,
                X = moveEndX - startX,
                Y = moveEndY - startY
            //判断为touch
            if (X == 0 && Y == 0) {
                $('.c-navbar-item').removeClass('active')
                $('.c-goods-item').hide()
                $(this).addClass('active')
                $('.c-goods-item').eq(index).show()
                myScroll.scrollTo(0, 0);
            }
        });
    })


    /**
     * 
     * ===================================================
     * 点击购物车图标
     * ===================================================
     * 
     */
    

    function addAmount(o){

        var price = parseFloat( o.closest('.goods-line').find('.goods-price i').text() ),
            n = parseInt($('.selectprice .num').text()),
            totalPrice = parseFloat($('.selectprice .tot').text()),
            increasePrice = price * 1

            n ++
            totalPrice += increasePrice

            $('.selectprice .num').text(n)
            $('.selectprice .tot').text(totalPrice.toFixed(2))
            console.log(price)
    
    }

    // function calcPrice(price){
    //     return price * 1
    // }

    //点击购物车图片触发弹出框
    $('.btn-goods').on('click', function(){
        addAmount($(this))
    })

})