/**
 * Created by Liu Xinggang
 */

//上传身份证
var upload = function (c, d, e) {
    "use strict";
    var $c = document.querySelector(c),
        $d = document.querySelector(d),
        $e = document.querySelector(e),
        file = $c.files[0],
        reader = new FileReader();
    reader.readAsDataURL(file);
    reader.onload = function (e) {
        $d.setAttribute("src", e.target.result);

        var str = $($e).val()
        str += ' ' + file.name
        $($e).val(str)
    };
};

//切换弹层
function showLayer() {
    $('.layer-toggle').addClass('show')
    $('.i-layer').addClass('show')
    $('html, body').on('touchmove', function(){
        return false
    })
}

$('#id-card').on('click', function () {
    showLayer()
})

$('.btn-close-layer').on('click', function () {
    hideLayer()
})

$('.btn-sure').on('click', function () {
    hideLayer()
})

//选择投资者身份
$('.invest-role-group .invest-role-item').each(function(){
    $(this).on('click', function(){
        $(this).addClass('active').siblings().removeClass('active')
    })
})

//发送验证码
var seconds = 120, timer = null
$('.btn-get-yzm').on('click', function(){
    timer = setInterval(function(){
        if(seconds <=0 ) {
            clearInterval(timer)
            $('.btn-get-yzm').text('发送验证码').attr('disabled', false)
            return false
        }
        seconds --
        $('.btn-get-yzm').text('重新发送('+seconds+')s').attr('disabled', true)
    }, 1000)
})


//城市三级联动
$("#city").click(function (e) {
    SelCity(this, e)
    $('.i-layer').addClass('show')
    $('._citys').toggleClass('active')
    $('html,body').css('overflow', 'hidden')
})

$('.i-layer').on('click', function(){
    hideLayer()
})

