var investor = {
	flag:true,
	showSlideNav:function(){
		$(".layermask").css("visibility", "visible");
		$("html,body").css("overflow", "hidden");
		$("#slideNav-box").slideDown();
	},
	hideSlideNav:function(){
		$(".layermask").css("visibility", "hidden");
		$("html,body").css("overflow", "auto");
		$("#slideNav-box").slideUp();
	},
	slideNavTab:function(){
		$("#slideNav-box").children("li").click(function() {
			$(this).addClass("cur").siblings().removeClass("cur");
		})
	},
	clickLayer:function(){
		$(".layermask").click(function() {
			investor.flag = true;
			investor.hideSlideNav();
		
		})
	},
	SlideNav:function(){
		$(".self-sale-classify").click(function() {
			if(investor.flag) {
				investor.flag = false;
				investor.showSlideNav();
				investor.slideNavTab();
			} else {
				investor.flag = true;
				investor.hideSlideNav();
			}
		})
	}
}
var invest = {
	//数量增减
	changeAmount:function(n,f){
		f == true ? n++ : n--
	    n = n <= 600 ? 600 : n
	    return n;
	},
	//数量+
	plus:function(){
		$('.icon-plus').on('click', function () {
		    var val = $('.btn-amount-val').val(),
		        n = invest.changeAmount(val, true)
		    $('.btn-amount-val').val(n)
		})
	},
	//数量-
	minus:function(){
		$('.icon-minus').on('click', function () {
		    var val = $('.btn-amount-val').val(),
		        n = invest.changeAmount(val)
		    $('.btn-amount-val').val(n)
		})
	}
}
var depot = {
	showProBox:function(){
		$(".layermask").css({"visibility":"visible","z-index":"99"});
		$("html,body").css("overflow", "hidden");
		$(".self-pro-alert-box").slideDown();
	},
	hideProBox:function(){
		$(".layermask").css({"visibility":"hidden","z-index":"1"});
		$("html,body").css("overflow", "auto");
		$(".self-pro-alert-box").slideUp();
	},
	proListClick:function(){
		$(".self-sale-proList-item").children("li").click(function(){
			depot.showProBox();
		})
	},
	clickLayer:function(){
		$(".layermask").click(function() {
			depot.hideProBox();
		})
	},
	btnClose:function(){
		$(".btn-close-layer").click(function(){
			depot.hideProBox();
		})
	}
}
