//------------订单---------------//
var order = {
	//	导航栏
	orderSubNav: function(ele, tablist) {
		$(ele).children("li").click(function() {
			var ind = $(this).index();
			$(this).addClass("cur").siblings().removeClass("cur");
			$(tablist).children().eq(ind).css({"height":"auto","overflow": "auto"}).siblings().css({"height":"0","overflow": "hidden"})
		})
	},
	swipe:function(){
		var swiper = new Swiper('.swiper-container', {
			slidesPerView: 4,
			pagination: {
				el: '.swiper-pagination',
				clickable: true,
			},
		});
	},
	//	删除订单
	deleteOrder: function() {
		$(".order_btn_delete").click(function() {
			$(this).parent().parent(".order_box").remove();
		})
	},
	//	单选框
	icheck: function(ele) {
		$(ele).iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
			increaseArea: '20%'
		});
	},
	//	删除订单弹出层
	removeOrder: function() {
		$(".order_remove_btn").click(function() {
			$(".remove_order").slideDown(function() {
				$(this).find(".remove_close_btn").click(function() {
					$(".remove_order").slideUp()
				})
			});
		})
	},
	//取消原因选择
	reasonTab:function(){
		$(".reason_list").children("li").click(function(){
			$(this).children(".cryptonym-btn").addClass("active");
			$(this).siblings().children(".cryptonym-btn").removeClass("active");
		})
	}
}
order.deleteOrder()
order.orderSubNav("#myOrder-subnav", "#order-content")
order.orderSubNav("#myOrder-subnav", "#apply-content")
order.removeOrder();
order.reasonTab();

//------------售后---------------//
var apply = {
	applyTab: function(ele, className) {
		$(ele).children().click(function() {
			$(this).addClass(className).siblings().removeClass(className);
		})
	},
	//清空搜索框
	clearInput:function(){
		$(".apply-clear-btn").click(function(){
			$(this).siblings("#apply-search-input").val("");
		})
	}
}

apply.applyTab(".apply-type-list", "cur");
apply.applyTab(".apply-return-mode", "cur");
apply.clearInput();

//自管投资人销售订单
var saleOrder={
	//显示确认收货操作
	shipClick:function(){
		$(".Shipping-Handler").click(function(){
			saleOrder.showShip();
		});
	},
	//显示确认发货
	showShip:function(){
		$(".layermask").css({"visibility": "visible","opacity":"1"});
		$("html,body").css("overflow","hidden");
		$(".deliver-alert-box").show();
		saleOrder.openList();
		saleOrder.companyListTab();
		saleOrder.hideCompanyList();
		saleOrder.closeCompany();
	},
	//隐藏确认发货
	hideShip:function(){
		$(".layermask").css({"visibility": "hidden","opacity":"0"});
		$("html,body").css("overflow","auto");
		$(".deliver-alert-box").hide();
	},
	//关闭发货弹出层
	closeDeliver:function(){
		$(".close-deliver-alert-box").click(function(){
			saleOrder.hideShip();
		})
	},
	//点击遮罩层
	clickLayer:function(){
		$(".layermask").click(function(){
			saleOrder.hideShip();
		})
	},
	//显示快递公司列表
	showCompanyList:function(){
		$(".delivery-company-list").show();	
		saleOrder.companyListTab();
	},
	//隐藏快递公司列表			
	hideCompanyList:function(){
		$(".delivery-company-list").hide();	
	},
	//快递公司列表切换
	companyListTab:function (){
		$(".company-list").children("li").click(function(){
			$(this).addClass("cur").siblings().removeClass("cur");
			var txt = $(this).text();
			$("#company-name").val(txt);
			saleOrder.hideCompanyList();
		});
	},
	//打开快递公司列表
	openList:function(){
		$(".company-name").click(function(){
			saleOrder.showCompanyList();
		});
	},
	//关闭快递公司列表
	closeCompany:function(){
		$(".btn-close-layer").click(function(){
			saleOrder.hideCompanyList();
		})
	}	
}

//	--------------------发票-------------------------//
var invoice = {
	invoiceBtn: function() {
		$(".invoice-con-btns").children().click(function() {
			$(this).addClass("cur").siblings().removeClass("cur")
		})
	}, //发票内容按钮
	invoiceRadio: function() {
		$(".invoice-title").children(".invoice-radio").click(function() {
			var ind = $(this).index();
			console.log(ind);
			ind === 2 ? invoice.showCompany() : invoice.hideCompany();
			$(this).addClass("cur").siblings(".invoice-radio").removeClass("cur");
		})
	}, //个人和单位发票切换
	showCompany: function() {
		$(".company-name").show();
		$(".invoice-identification-number").show();
	}, //显示公司名称和识别号
	hideCompany: function() {
		$(".company-name").hide();
		$(".invoice-identification-number").hide();
	} //隐藏公司名称和识别号
}
//--------------------------收银台---------------------//
var cashier = {
	paymentSystem: function() {
		$(".payment-system-list").children("li").click(function() {
			$(this).addClass("active").siblings().removeClass("active");
			var payment = $(this).find(".payment-title").text();
			$(".payment-text").text(payment);
		})
	},
	count: function() {
		var intDiff = parseInt(7200); //倒计时总秒数量
		function timer(intDiff) {
			window.setInterval(function() {
				var day = 0,hour = 0,minute = 0,second = 0; //时间默认值		
				if(intDiff > 0) {
					hour = Math.floor(intDiff / (60 * 60)) - (day * 24);
					minute = Math.floor(intDiff / 60) - (day * 24 * 60) - (hour * 60);
					second = Math.floor(intDiff) - (day * 24 * 60 * 60) - (hour * 60 * 60) - (minute * 60);
				}
				if(minute <= 9) minute = '0' + minute;
				if(second <= 9) second = '0' + second;
				$('#hour_show').html('<s id="h"></s>' + hour);
				$('#minute_show').html('<s></s>' + minute);
				$('#second_show').html('<s></s>' + second);
				intDiff--;
			}, 1000);
		}
		$(function() {
			timer(intDiff);
		});
	}
}

//-----------------------------自提---------------------------//
var deliver = {
	ind:"",
	arr:function(){
		return $(".self-lifting-time");
	},
	deliverTime: function() {
		$(".Self-lifting-time").each(function(index){
			$(this).click(function(){
				deliver.ind = index;
				deliver.showLiftingTime();
			})
		})		
	},
	closeLayer: function() {
		$(".btn-close-layer").click(function() {
			deliver.hideLiftingTime();
		})
	},
	showLiftingTime:function(){
		$("#lifting-time-box-wrap").show();
		$("html,body").css("overflow", "hidden");
		deliver.getDeliverTime()
	},
	hideLiftingTime:function(){
		$("#lifting-time-box-wrap").hide();
		$("html,body").css("overflow", "auto");
	},
	getDeliverTime: function() {
		$("#lifting-time-list").children("li").click(function() {
			$(this).addClass("cur").siblings().removeClass("cur");
			var txt = $(this).text();
			var Arr = deliver.arr();
			$(Arr[deliver.ind]).val(txt);
		})
	},
	deliverTab:function(ele){
		$(ele).children().click(function(){
			$(this).addClass("cur").siblings().removeClass("cur");
		})
	}
}