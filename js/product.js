/**
 * Created by Liu Xinggang
 */
$(function () {

    //商品轮播图
    var swiper = new Swiper('.p-tab-list .swiper-container', {
        loop: true,
        pagination: {
            el: '.swiper-pagination',
            clickable: true,
        },
        on: {
            init: function () {
                $('.p-current').text(this.realIndex + 1)
                $('.p-count').text(this.slides.length / 2)
            },
            slideChangeTransitionStart: function () {
                $('.p-current').text(this.realIndex + 1)
            },
        },
    })

    //整体内容轮播图
    var swiperContent = new Swiper('.p-container .swiper-container', {
        autoHeight: true,
        on: {
            slideChangeTransitionStart: function () {
                tabChange($('.p-tab-item').eq(this.activeIndex), 'active')
                $('html, body').animate({ scrollTop: 0 }, 500)
            },
        },
    })

    //顶部 ‘商品，详情，评价，采购’ 切换
    function swipeTo(o, index) {
        $('html, body').animate({ scrollTop: 0 }, 500)
        o.slideTo(index, 500, false)
    }

    //顶部 ‘商品，详情，评价，采购’高亮
    function tabChange(o, str) {
        o.addClass(str).siblings().removeClass(str)
    }

    $('.p-tab-item').each(function (index) {
        $(this).on('click', function () {
            tabChange($(this), 'active', )
            tabChange($('.p-tab-list').eq(index), 'current')
            swipeTo(swiperContent[0], index)
            console.log(index)
        })
    })

    //选择颜色，尺寸，品种
    $('.p-s-style').each(function (index) {
        $(this).on('click', function () {
            tabChange($(this), 'active')
        })
    })

    //数量增减
    function changeAmount(n, f) {
        f == true ? n++ : n--
        n = n <= 1 ? 1 : n
        return n;
    }

    //数量+
    $('.btn-amount-add').on('click', function () {
        var val = $('.btn-amount-val').val(),
            n = changeAmount(val, true)
        $('.btn-amount-val').val(n)
    })

    //数量-
    $('.btn-amount-del').on('click', function () {
        var val = $('.btn-amount-val').val(),
            n = changeAmount(val)
        $('.btn-amount-val').val(n)
    })

    //显示规格弹出层
    function showSpec(obj, mask, class1, class2, class3) {
        obj.removeClass(class2).addClass(class1).show()
        mask.addClass(class3)
        $('html,body').css('overflow', 'hidden')
    }

    //隐藏规格弹出层
    function hideSpec(obj, mask, class1, class2, class3) {
        obj.removeClass(class2).addClass(class1).hide(500)
        mask.removeClass(class3)
        $('html,body').css('overflow', 'initial')
    }


    //商品规格弹出
    $('.btn-spec').on('click', function () {
        showSpec($('.p-spec'), $('.p-layer'), 'spec-menu-show', 'spec-menu-hide', 'show')
    })

    //商品规格收起
    $('.p-layer').on('click', function () {
        hideSpec($('.p-spec'), $('.p-layer'), 'spec-menu-hide', 'spec-menu-show', 'show')
    })

    $('.p-s-head').find('*').on('touchmove', function () {
        return false
    })

    //商品详情评价页面
    $('.radio-span').each(function () {
        $(this).click(function () {
            $(this).toggleClass('active')

            var obj = $('.radio-span').children('input')
            if (obj.attr("checked") == 'checked') {
                obj.removeAttr("checked");
            } else {
                obj.attr("checked", true);
            }

        })
    })

    //点击评价类型
    $('.p-e-types dd').each(function (index) {
        $(this).on('click', function () {
            tabChange($(this), 'active')
        })
    })

    //点击评价下的箭头
    $('.btn-e-more').on('click', function () {
        $(this).closest('.p-e-types').find('dd').filter(function (i, item) { return i > 3 }).fadeToggle()
    })

    //右上角弹出菜单
    $('.btn-partake').on('click', function (e) {
        e.stopPropagation()
        $('.shortcut').toggleClass('active')
    })

    $('html,body').on('click', function () {
        $('.shortcut').removeClass('active')
    })


    //关闭弹层layer-p
    function closeLayerP() {
        $('.layer-p').removeClass('show')
        $('.p-layer').removeClass('show')
        $('html,body').css('overflow', 'initial')
    }

    $('.btn-close-layer, .layermask').on('click', function () {
        closeLayerP()
    })

    $('.btn-sure').on('click', function () {
        closeLayerP()
    })

    //弹出客服
    $('.cus-ser').on('click', function () {
        $('.layer-service').addClass('show')
        $('.p-layer').addClass('show')
        $('html,body').css('overflow', 'hidden')
    })

    //服务说明弹出框
    $('.btn-explain').on('click', function () {
        $('.layer-explain').addClass('show')
        $('.p-layer').addClass('show')
        $('html,body').css('overflow', 'hidden')
    })

    //地址弹出
    $('.btn-dispatching').on('click', function () {
        $('.layer-dispatching').addClass('show')
        $('.p-layer').addClass('show')
        $('html,body').css('overflow', 'hidden')
    })

    //地址弹出 ’点击选择已有地址‘
    $('.addr-row').on('click', function () {
        tabChange($(this), 'active')
        var index = $(this).index() || 0,
            text = $('.locate-addr').eq(index - 1).text()
        $('#city').val(text)
        closeLayerP()
    })

    //选择其他地址
    $("#btn-new-addr").click(function (e) {
        $('.layer-p').removeClass('show')
        SelCity(this, e)
        $('.i-layer').addClass('show')
        $('._citys').toggleClass('active')
        $('html,body').css('overflow', 'hidden')
    })

    //加入购物车 ‘+1出现、消失’
    var timer = null
    $('.btn-join').on('click', function () {
        $('.add_num').addClass('show')
        if (timer) {
            clearTimeout(timer)
        }
        tiemr = setTimeout(function () {
            $('.add_num').removeClass('show')
        }, 2000)
    })

    //收藏
    $('.action-collect').on('click', function () {
        layer.open({
            content: '收藏成功'
            , skin: 'msg'
            , time: 1 //1秒后自动关闭
        });
    })
})