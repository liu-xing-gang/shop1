/**
 * Created by Liu Xinggang
 */
$(function(){
    //复位所有效果
    function recovery () {
        $('.s-filter').removeClass('show')
        $('.s-controlcont').css({'z-index': 'initial', top: 0, width: '100%', height: 'auto', left: 'initial'})
        $('.layermask').css({'z-index': '5'})
        $('.s-controlcont-i').not('.s-filter').hide()
        $('.layermask').css({opacity: 0, visibility: 'hidden'})
        $('html,body').removeClass('overflow-hide')
    }

    //关闭弹层
    $('.layermask').on('click', function(){
        recovery()
    })
    
    //点击配送、采购时，去点击了搜索,需恢复所有状态
    $('.i-search input').on('click', function (e) {
        recovery()
    })

    //点击对应类型,绿色显示当前文字
    function reaction(o, all){
        all.removeClass('active')
        o.addClass('active')
    }

    //零利润超市
    $('#zeroprofit').on('click', function(){
        reaction($(this), $('.s-controlbar-item'))
        recovery()
    })

    //配送
    $('#dispatching').on('click', function(){
        reaction($(this), $('.s-controlbar-item'))
        recovery()
        reactionSame($(this))
    })

    //采购
    $('#purchase').on('click', function(){
        reaction($(this), $('.s-controlbar-item'))
        recovery()
        reactionSame($(this))
    })

    //配送、采购子菜单显示
    function reactionSame(o){
        var index = o.parent().index()
        $('html,body').addClass('overflow-hide')
        $('.layer-cont ul').eq(index).show().siblings('ul').hide()
        $('.layermask').css({opacity: 1, visibility: 'visible'})
        $('.s-controlcont').css({'z-index': '997', top: $('.header').height() + $('.s-controlbar').height(), height: 'auto', left: 'initial','position': 'absolute'})
    }

    //配送、采购子菜单下 对勾点击更换文本
    $('.same-styele').each(function(i){
        $(this).children('li').each(function(index){
            $(this).click(function(){ 
                var _index = $(this).parent().index()
                $('.s-controlbar-item').eq(_index).find('.s-p-name').text($(this).text())
                recovery()  
            })
        })
    })

    //筛选
    $('#screen').on('click',function(){
        reaction($(this), $('.s-controlbar-item'))
        recovery()
        reactionScreen()
    })

    //筛选 '子选项显示'
    function reactionScreen(){
        $('html,body').addClass('overflow-hide')
        $('.s-controlcont').css({'z-index': '9999', 'height': '100%', 'width': '6.7rem', 'left': '.8rem', 'position': 'fixed'})
        $('.layermask').css({'z-index': '9997'})
        $('.s-filter').addClass('show')
        $('.layermask').css({opacity: 1, visibility: 'visible'})
    }

    //筛选 '点击确定'
    $('.s-confirm').on('click', function(){
        recovery()  
    })

    // 筛选 '点击全部'
    $('.grid-box').each(function(){
        $(this).find('.btn-switch').on('click', function(){
            $(this).closest('.grid-box').find('.grid-cell').filter(function(i,item){return i>2}).fadeToggle()
        })
    })

    //筛选 '点击有货'
    $('.in-stock').on('click', function(){
        reaction($(this), $('.in-stock'))
    })

    //回到顶部
    $('#back-to-top').click(function () { $('html,body').animate({ scrollTop: '0px' }, 500); });

    //阻止滑动遮罩层的默认
    $('.layermask').on('touchmove', function(e){
        e.stopImmediatePropagation()
        e.stopPropagation()
        e.preventDefault()
    })
})