/**
 * Created by Liu Xinggang
 */
$(function () {

    //显示搜索历史
    function showHistory () {
        location.hash = '#historys'
        $('.i-head').addClass('active')
        $('#historys').css({opacity: 1, visibility: 'visible', 'height': '100%'})
        $('html,body, .container').css('overflow', 'hidden')
    }

    //隐藏搜索历史
    function hideHistory () {
        $('.i-head').removeClass('active')
        $('#historys').css({opacity: 0, visibility: 'hidden','height': '0'})

        $('html,body').css('overflow', 'initial')
        $('.container').css({'overflow-y': 'scroll', '-webkit-overflow-scrolling': 'touch'})
    }

    //弹出键盘
    $('.i-search input').on('touchstart', function (e) {
        showHistory()
    })

    $('.i-search input').on('click', function (e) {
        showHistory()
    })

    //解决ios下input获得焦点fixed定位失效
    $('html, body').on('touchmove', function(){
        $('#keyword').blur()
    })


    //前进后退
    $(window).on('hashchange', function () {
        if (location.hash == '') {
            hideHistory()
        } else if (location.hash == '#historys') {
            showHistory()
        }
    })

    $(".btn-search-close").on({
        touchstart: function() {
            $('#keyword').val('')
            $('#keyword').focus()
        },
        click: function() {
            $('#keyword').val('')
            $('#keyword').focus()
        }
    });

    //返回搜索列表页
    $('.btn-goback').on('click', function () {
        history.go(-1)
        hideHistory()
    })

    //搜索请求数据
    $("#keyword").on('keypress', function (e) {
        var keycode = e.keyCode
        var searchName = $(this).val();
        if (keycode == '13') {
            //按下回车键
            e.preventDefault();
            //请求搜索接口  
            alert('按下了回车键！')
        }
    })
})