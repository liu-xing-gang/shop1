//右上角点击效果
function setBtn() {
	$(".set-btn").click(function() {
		$(this).siblings(".set-popup").toggle();
	})
}
setBtn();
//分享
function share(){
	$(".mem-shareApp").click(function(){
		$(".share-box").show();
		$(".share-box").find(".share-remove").click(function(){
			$(".share-box").hide();
		})
	})
}
share();
//返回顶部效果
function backTop() {
	//首先将#back-to-top隐藏
	$("#back-top").hide();
	//当滚动条的位置处于距顶部100像素以下时，跳转链接出现，否则消失
	$(function() {
		$(window).scroll(function() {
			if($(window).scrollTop() > 100) {
				$("#back-top").fadeIn(1000);
			} else {
				$("#back-top").fadeOut(1000);
			}
		});
		//当点击跳转链接后，回到页面顶部位置
		$("#back-top").click(function() {
			$('body,html').animate({
					scrollTop: 0
				},
				1000);
			return false;
		});
	});
}
backTop()
//导航
function nav() {
	$(".collection-nav").children("li").click(function() {
		var eleName = $(this).attr("class");
		if(eleName.indexOf("classify") > 0) {
			$(this).addClass("cur").siblings().removeClass("cur");
			$(".classify-subnav").show();
			$('body').css("overflow", "hidden")
		} else {
			$(this).addClass("cur").siblings().removeClass("cur");
			$(".classify-subnav").hide();
			$('body').css("overflow", "auto")
		}
	})
}
nav();
//更换头像
function changeImg() {
	$(".user-pic").click(function() {
		$(".change-user-img").show();
		$(".exit-btn").click(function() {
			$(".change-user-img").hide();
		})
	});

};
changeImg();
//收藏页二级导航
function subNav(ele){
	$(ele).children().click(function(){
		$(this).addClass("cur");
	})
}
subNav(".subnav-list");
function resetSubNav(){
	$(".reset-btn").click(function(){
		$(this).siblings(".subnav-list").children("li").removeClass("cur");
	});
	$(".ok-btn").click(function(){
		$(".classify-subnav").hide();
	})
}
resetSubNav();