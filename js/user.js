//选项卡
function tab(ele) {
	$(ele).children().eq(0).addClass("cur");
	$(ele).siblings(".tab-item").eq(0).show();
	$(ele).children("li").click(function() {
		var ind = $(this).index();
		$(this).addClass("cur").siblings().removeClass("cur");
		$(this).parent().siblings(".tab-item").eq(ind).show().siblings(".tab-item").hide();
	})
}
tab(".login-type");
//表单验证
function FormVerification() {
	$(".login-btn").click(function() {
		var $phone_input = $(this).siblings(".input-list").find(".login-phone")
		var phone_str = $phone_input.val();
		if(!(/^1[3|4|5|8][0-9]\d{4,8}$/.test(phone_str))) {
			alert("请输入正确的手机号码");
			$phone_input.focus();
			return false;
		}
	})
}
FormVerification()
//显示密码
function showPwd() {
	$(".eyes").click(function() {
		var str = $(this).siblings(".login-pwd").attr('type');
		str === 'password' ? $(this).siblings(".login-pwd").attr('type', 'text') : $(this).siblings(".login-pwd").attr('type', 'password')
	})
}
showPwd();

//注册页面按钮变化
function btnChange() {
	$(".login-phone").click(function() {
		$(this).parent().siblings(".login-btn").removeClass("regist-btn");
	})
};
btnChange()
//发送验证码后倒计时
function postCode() {
	var countdown = 60;
	$(".post-code").click(function sendemail() {
		var obj = $(this);
		settime(obj);
		$(this).siblings(".login-code").focus();
	});

	function settime(obj) { //发送验证码倒计时
		if(countdown == 0) {
			obj.attr('disabled', false);
			//obj.removeattr("disabled"); 
			obj.val("发送验证码");
			countdown = 60;
			return;
		} else {
			obj.attr('disabled', true);
			obj.val("重新发送(" + countdown + ")");
			countdown--;
		}
		setTimeout(function() {
			settime(obj)
		}, 1000)
	}
}
postCode();

function he() {
	var he = $(window).height();
	$(".login-content").css("min-height", he);
}
window.addEventListener('load', he)
window.addEventListener('resize', he)

var order = {
//	导航栏
	orderSubNav: function() {
		$("#myOrder-subnav").children().eq(0).addClass("cur");
		$("#order-content").children(".my_order").eq(0).show();
		$("#myOrder-subnav").children().click(function() {
			var ind = $(this).index();
			$(this).addClass("cur").siblings().removeClass("cur");
			$(this).parent().siblings("#order-content").children(".my_order").eq(ind).show().siblings(".my_order").hide()
		})
	},
//	删除订单
	deleteOrder: function() {
		$(".order_btn_delete").click(function() {
			$(this).parent().parent(".order_box").remove();
		})
	},
//	单选框
	icheck: function(ele) {
		$(ele).iCheck({
			checkboxClass: 'icheckbox_square-green',
			radioClass: 'iradio_square-green',
			increaseArea: '20%'
		});
	},
//	删除订单弹出层
	removeOrder: function() {
		$(".order_remove_btn").click(function() {
			$(".remove_order").slideDown(function() {
				$(this).find(".remove_close_btn").click(function() {
					$(".remove_order").slideUp()
				})
			});
		})
	}
}
order.deleteOrder()
order.orderSubNav()
order.removeOrder()